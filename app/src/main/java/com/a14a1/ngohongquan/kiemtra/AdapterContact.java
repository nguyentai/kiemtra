package com.a14a1.ngohongquan.kiemtra;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by nguyenthetai on 20/03/2018.
 */

public class AdapterContact extends ArrayAdapter<Contact> {

    public AdapterContact(@NonNull Context context, int resource, @NonNull List<Contact> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView== null){
            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_contact_layout,null);
            viewHolder.name = convertView.findViewById(R.id.name);
            viewHolder.phoneNumber = convertView.findViewById(R.id.phone);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Contact contact = getItem(position);
        viewHolder.name.setText(contact.getName());
        viewHolder.phoneNumber.setText(contact.getPhoneNumber());
        return convertView;
    }

    class ViewHolder{
        TextView name;
        TextView phoneNumber;
    }
}
