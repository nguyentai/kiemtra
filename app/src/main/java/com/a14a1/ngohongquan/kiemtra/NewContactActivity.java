package com.a14a1.ngohongquan.kiemtra;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;

/**
 * Created by nguyenthetai on 20/03/2018.
 */

public class NewContactActivity extends Activity {

    EditText name;
    EditText phoneNumber;
    Button newContact;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.new_contact_layout);
        name = findViewById(R.id.name);
        phoneNumber = findViewById(R.id.phone);
        newContact = findViewById(R.id.newContact);

        newContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(check()){
                    Contact contact = new Contact();
                    contact.setName(name.getText().toString().trim());
                    contact.setPhoneNumber(phoneNumber.getText().toString().trim());
                    Intent intent = new Intent();
                    intent.putExtra("contact", (Serializable) contact);
                    setResult(RESULT_OK,intent);
                    finish();
                }
            }
        });

    }

    private boolean check(){
        if(!TextUtils.isEmpty(name.getText().toString().trim()) && !TextUtils.isEmpty(phoneNumber.getText().toString().trim()))
            return true;
        return false;
    }
}
